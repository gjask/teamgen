
FROM python:3.10 AS builder

WORKDIR /source

COPY requirements.txt setup.py MANIFEST.in ./
RUN pip install --user -r requirements.txt && \
    pip install --user gunicorn

COPY ./teamgen ./teamgen
RUN pip install --user ./


FROM python:3.10-slim

EXPOSE 80
WORKDIR /source

COPY --from=builder /root/.local /root/.local
COPY Dockerfile ./

ENV PATH="/root/.local/bin:${PATH}" \
    TZ='Europe/Prague' \
    FLASK_APP='teamgen.app:make_app()' \
    FLASK_ENV='production'

CMD [ \
    "gunicorn", \
    "--bind=:80", \
    "--worker-tmp-dir=/dev/shm", \
    "--workers=1", \
    "--threads=2", \
    "--worker-class=gthread", \
    "--error-logfile=-", \
    "--access-logfile=-", \
    "--access-logformat='%(h)s %(l)s %(u)s %(t)s \"%(r)s\" %(s)s %(b)s \"%(f)s\" \"%(a)s\"' %(M)s", \
    "--max-requests=1", \
    "teamgen.app:make_app()" \
    ]
