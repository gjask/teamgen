from os.path import abspath, join, dirname
from setuptools import setup, find_packages

from teamgen.version import version

here = abspath(dirname(__file__))
requirements = open(join(here, "requirements.txt"))

setup(
    name="teamgen",
    version=version,
    author="Jan Skrle",
    author_email="j.skrle@gmail.com",
    packages=find_packages(exclude=("tests", )),
    include_package_data=True,
    install_requires=requirements.readlines(),
    zip_safe=False,
    test_suite="tests",
)
