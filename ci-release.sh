#!/bin/sh

version_file=$(realpath $(dirname $0)/teamgen/version.py)

err(){
  echo "$@" >&2
  exit 1
}

tag="$1"
version=$(echo "$tag" |  grep -Eo '[0-9]+\.[0-9]+\.[0-9]+')
[ -z "$version" ] && err "You have to pass valid tag as an argument!"

echo "version = '${version}'" > $version_file
echo "$version"
