from ..core.models import Game
from ..core.team_maker import TeamMaker
from ..sheet import (
    SheetDAO,
    ContestantDuplicity,
    MalformedTableData,
    UnknownContestant,
    MissingContestantList,
)

from enum import IntEnum, IntFlag, auto

from flask.blueprints import Blueprint
from flask.globals import current_app, request
from flask.helpers import flash, url_for
from flask.templating import render_template
from flask_wtf.form import FlaskForm as Form
from pygsheets import authorize
from werkzeug.utils import redirect
from wtforms.fields import StringField, IntegerField, SelectField
from wtforms.validators import DataRequired, NumberRange


root = Blueprint("core", __name__)


def get_sheet_dao():
    client = authorize(
        service_file=current_app.settings.teamgen.service_key_file,
    )

    return SheetDAO.get_by_key(
        pygsheets_client=client,
        sheet_key=current_app.settings.teamgen.sheet_key,
    )


class Step(IntFlag):
    new_form = auto()
    fill_form = auto()
    create = auto()
    fill_in = auto()


class Act(IntEnum):
    display = 0
    create = Step.new_form | Step.create | Step.fill_in
    create_empty = Step.new_form | Step.create
    fill_in = Step.fill_form | Step.fill_in


@root.errorhandler(ContestantDuplicity)
@root.errorhandler(MalformedTableData)
@root.errorhandler(UnknownContestant)
@root.errorhandler(MissingContestantList)
def error_handler(error):
    return render_template(
        "error.html",
        error_class_name=error.__class__.__name__,
        exception=error,
    )


@root.route(
    "/",
    defaults={"action": Act.display}
)
@root.route(
    "/create",
    methods=("GET", "POST"),
    defaults={"action": Act.create},
    endpoint="create",
)
@root.route(
    "/create-empty",
    methods=("GET", "POST"),
    defaults={"action": Act.create_empty},
    endpoint="create_empty",
)
@root.route(
    "/fill-in",
    methods=("GET", "POST"),
    defaults={"action": Act.fill_in},
    endpoint="fill_in",
)
def index(action):
    if action and request.method == "GET":
        return redirect(url_for(".index", action=Act.display))

    sheet = get_sheet_dao()
    parsed_games = sheet.load_all()

    new_form = NewGameForm()
    fill_form = FillInGameForm()

    fill_form.update_game_list(parsed_games.games)

    if (
        (action & Step.new_form and new_form.validate())
        or (action & Step.fill_form and fill_form.validate())
    ):
        if action & Step.create:
            game = Game.create_empty(
                name=new_form.name.data,
                number_of_teams=new_form.number_of_teams.data,
                team_prefix="Tým ",
            )

            msg = f"Nový list hry vytvořen: {game.name}"
            worksheet_id = None

        else:
            game = parsed_games.get_game_by_worksheet_id(
                fill_form.worksheet_id.data,
            )

            msg = f"Doplněn list hry: {game.name}"
            worksheet_id = game.worksheet_id

        if action & Step.fill_in:
            TeamMaker(
                contestants=parsed_games.get_active_contestants(),
                prev_games=set(parsed_games.games) - {game},
            ).fill_in_players(game)

        sheet.write_game(game, worksheet_id=worksheet_id)
        flash(msg, "success")
        return redirect(url_for(".index", action=Act.display))

    return render_template(
        "core/index.html",
        gamestate=parsed_games,
        new_game_form=new_form,
        fill_in_game_form=fill_form,
        sheet_url=sheet.sheet.url,
    )


@root.route("/leaderboard", methods=("POST", ))
def leaderboard():
    sheet = get_sheet_dao()
    parsed_games = sheet.load_all()

    sheet.write_score(parsed_games)

    flash("Výslekdy přepočítány", "success")
    return redirect(url_for(".index"))


@root.route("/affinity")
def affinity():
    sheet = get_sheet_dao()
    parsed_games = sheet.load_all()
    team_maker = TeamMaker(parsed_games.contestants, parsed_games.games)
    affinities = team_maker.get_contestant_affinities()

    return render_template(
        "core/affinity.html",
        contestants=[person for person in parsed_games.contestants if not person.hide],
        affinities=affinities,
        max_affinity=max(affinities.values()),
    )


class NewGameForm(Form):
    name = StringField(
        validators=[
            DataRequired("Název hry je povinný"),
        ],
    )
    number_of_teams = IntegerField(
        validators=[
            DataRequired("Počet týmů je povinný"),
            NumberRange(min=1, message="Počet týmů musí být kladné celé číslo"),
        ],
    )


class FillInGameForm(Form):
    worksheet_id = SelectField(
        coerce=int,
        validators=[
            DataRequired("Hra je povinná"),
        ],
    )

    def update_game_list(self, games):
        self.worksheet_id.choices = [
            (game.worksheet_id, f"{i}: {game.name}")
            for i, game in enumerate(games, start=1)
            if not game.scores
        ]
