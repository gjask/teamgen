from importlib import import_module

from flask import Flask
from flask.globals import request
from flask.helpers import flash, url_for
from flask.templating import render_template
from flask_wtf.csrf import CSRFProtect, CSRFError
from werkzeug.exceptions import HTTPException
from werkzeug.utils import redirect

from .config import Settings
from .lib.url import is_safe_url


def setup_csrf_protection(app):
    CSRFProtect(app)

    @app.errorhandler(CSRFError)
    def handle_invalid_token(err):
        flash("Platnost stránky vypršela. Zkuste to znovu", "danger")

        if not is_safe_url(request.referrer):
            return redirect(url_for("core.index"))

        return redirect(request.referrer)


def setup_settings(app):
    app.settings = Settings()
    app.config.update(
        {key.upper(): val for key, val in app.settings.flask.dict().items()}
    )


def setup_error_handlers(app):
    @app.errorhandler(HTTPException)
    def default_handler(exc):
        return render_template("error.html", exception=exc), exc.code


def setup_blueprints(app, blueprints):
    for bp in blueprints:
        module = import_module(".views.{}".format(bp), __package__)
        app.register_blueprint(module.root)


def make_app(testing=False):
    app = Flask(__package__)
    setup_settings(app)

    if app.env == "production":
        app.wsgi_app = PatchEnviron(
            app.wsgi_app,
            **app.settings.deployment.dict()
        )

    app.testing = testing

    setup_csrf_protection(app)
    setup_error_handlers(app)

    setup_blueprints(
        app,
        (
            "core",
        )
    )

    return app


class PatchEnviron:
    def __init__(self, wsgi_app, *, scheme, host, port, path="/"):
        self.wsgi_app = wsgi_app
        self.environ_patch = {
            "HTTP_HOST": "{}:{}".format(host, port),
            "SERVER_NAME": host,
            "SERVER_PORT": port,
            "SCRIPT_NAME": path,
            "wsgi.url_scheme": scheme,
        }

    def __call__(self, environ, start_response):
        environ.update(self.environ_patch)
        return self.wsgi_app(environ, start_response)
