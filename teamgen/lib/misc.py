
def strip_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text


def rank(iterable, key=None, reverse=False):
    prev_key = object()
    rnk = 0

    for cnt, item in enumerate(
            sorted(iterable, key=key, reverse=reverse),
            start=1,
    ):
        item_key = item if key is None else key(item)

        if prev_key != item_key:
            rnk = cnt

        prev_key = item_key
        yield rnk, item
