from urllib.parse import urlsplit, urljoin

from flask.globals import request


def is_safe_url(target):
    ref_url = urlsplit(request.host_url)
    test_url = urlsplit(urljoin(request.host_url, target))

    return (
        test_url.scheme in ('http', 'https')
        and ref_url.netloc == test_url.netloc
    )
