from pathlib import Path
from secrets import token_urlsafe

from pydantic import BaseModel, BaseSettings


class TeamgenSettings(BaseModel):
    sheet_key: str = ""
    service_key_file: Path = Path()


class FlaskSettings(BaseModel):
    log_level: str = "INFO"
    secret_key: str = token_urlsafe(16)

    session_cookie_secure: bool = True
    session_cookie_httponly: bool = True

    remember_cookie_secure: bool = True
    remember_cookie_httponly: bool = True

    recaptcha_private_key: str = ""
    recaptcha_public_key: str = ""


class DeploymentSettings(BaseModel):
    scheme: str = "https"
    host: str = "tp.skrle.cz"
    port: str = "443"
    path: str = "/"


class Settings(BaseSettings):
    teamgen: TeamgenSettings = TeamgenSettings()
    flask: FlaskSettings = FlaskSettings()
    deployment: DeploymentSettings = DeploymentSettings()
