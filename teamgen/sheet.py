from collections import Counter
from dataclasses import dataclass
from itertools import accumulate
from operator import itemgetter

from pygsheets.exceptions import WorksheetNotFound

from .core.models import Person, Team, Game
from .lib.misc import strip_prefix, rank


class MissingContestantList(ValueError):
    pass


class MalformedTableData(ValueError):
    pass


class ContestantDuplicity(ValueError):
    pass


class UnknownContestant(ValueError):
    pass


@dataclass(eq=False)
class ParsedPerson(Person):
    on_pause: bool = False
    hide: bool = False


@dataclass(eq=False)
class ParsedGame(Game):
    worksheet_id: int = None


@dataclass(eq=False)
class ParsedGameState:
    contestants: list[Person]
    games: list[ParsedGame]
    timestamp: str = None

    def get_game_by_worksheet_id(self, worksheet_id):
        for game in self.games:
            if game.worksheet_id == worksheet_id:
                return game

    def get_active_contestants(self):
        return (person for person in self.contestants if not person.on_pause)

    def get_scores(self):
        return sum(
            (game.scores for game in self.games),
            start=Counter({person: 0 for person in self.contestants}),
        )

    def get_score_progress(self):
        return list(accumulate(game.scores for game in self.games))

    def get_ranking(self):
        return [
            (rnk, person, score)
            for rnk, (person, score) in rank(
                filter((lambda x: not x[0].hide), self.get_scores().items()),
                key=itemgetter(1),
                reverse=True,
            )
        ]


class SheetDAO:
    CONTESTANT_WS = "Účastníci"
    SCORE_WS = "Pořadí"
    GAME_WS_PREFIX = "Hra:"

    CNTS_NAME = "Jméno"
    CNTS_SEX = "Sex"
    CNTS_ON_PAUSE = "Pauza"
    CNTS_HIDE = "Schovat"

    @classmethod
    def get_by_key(cls, pygsheets_client, sheet_key):
        return cls(
            sheet=pygsheets_client.open_by_key(sheet_key),
        )

    @classmethod
    def create(cls, pygsheets_client, title):
        raise NotImplementedError()

    def __init__(self, sheet):
        self.sheet = sheet

    def load_contestants(self):
        try:
            ws = self.sheet.worksheet_by_title(self.CONTESTANT_WS)

        except WorksheetNotFound:
            raise MissingContestantList(
                f"Could not fetch contestant list: '{self.CONTESTANT_WS}'"
            )

        contestants = {}

        for row in ws.get_all_records():
            try:
                person = ParsedPerson(
                    name=row[self.CNTS_NAME],
                    sex=row[self.CNTS_SEX],
                    on_pause=bool(row[self.CNTS_ON_PAUSE]),
                    hide=bool(row[self.CNTS_HIDE]),
                )

            except KeyError:
                raise MalformedTableData(
                    "Could not parse some property in contestant list."
                )

            if person.name in contestants:
                raise ContestantDuplicity(
                    f"Multiple records of '{person.name}' in contestants."
                )

            contestants[person.name] = person

        return contestants

    def load_games(self, contestants):
        games = []
        for ws in self.sheet.worksheets():
            if not ws.title.startswith(self.GAME_WS_PREFIX):
                continue

            games.append(self.parse_game(ws, contestants))

        return games

    def parse_game(self, ws, contestants):
        game = ParsedGame(
            name=strip_prefix(ws.title, self.GAME_WS_PREFIX).strip(),
            worksheet_id=ws.id,
        )

        data = ws.get_all_values(
            majdim="COLUMNS",
            include_tailing_empty=False,
            include_tailing_empty_rows=False,
        )

        for col_num, col in enumerate(data):
            if not col or not col[0]:
                continue

            team = Team(name=col[0])

            for row_num, name in enumerate(col[1:], 1):
                if not name:
                    continue

                try:
                    person = contestants[name]

                except KeyError:
                    raise UnknownContestant(
                        f"Person '{name}' from '{ws.title}' not in contestants."
                    )

                team.members.append(person)

                try:
                    game.scores[person] += int(data[col_num+1][row_num] or 0)

                except IndexError:
                    pass

                except ValueError:
                    raise MalformedTableData(
                        f"Could not parse score for '{name}' "
                        f"at '{ws.title}'"
                    )

            game.teams.append(team)

        return game

    def load_all(self):
        timestamp = self.sheet.updated

        contestants = self.load_contestants()
        games = self.load_games(contestants)

        return ParsedGameState(
            contestants=list(contestants.values()),
            games=games,
            timestamp=timestamp,
        )

    def write_game(self, game, spacing=3, worksheet_id=None):
        if worksheet_id is None:
            ws = self.sheet.add_worksheet(f"{self.GAME_WS_PREFIX} {game.name}")

        else:
            ws = self.sheet.worksheet("id", worksheet_id)
            ws.clear()

        for i, team in enumerate(game.teams):
            members = sorted(person.name for person in team.members)

            ws.update_col(
                index=i * spacing + 1,
                values=[team.name] + members
            )

    def write_score(self, parsed_games):
        try:
            ws = self.sheet.worksheet_by_title(self.SCORE_WS)

        except WorksheetNotFound:
            ws = self.sheet.add_worksheet(self.SCORE_WS)

        else:
            ws.clear()

        scored_games = [game for game in parsed_games.games if game.scores]

        ws.update_values(
            crange="A1",
            values=[
                ["#", "Jméno"] + [
                    game.name for game in scored_games
                ] + ["Celkem"]
            ] + [
                [f"{rnk}.", person.name] + [
                    game.scores[person]
                    for game in scored_games
                ] + [score]
                for rnk, person, score in parsed_games.get_ranking()
            ]
        )
