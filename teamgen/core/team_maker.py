from collections import Counter
from itertools import chain, combinations
from random import sample


class Affinities(Counter):
    """
    Counter for how often were any two persons in same team.
    """
    @classmethod
    def from_teams(cls, teams):
        return cls(
            frozenset(pair) for pair in chain.from_iterable(
                combinations(team.members, 2) for team in teams
            )
        )

    def pair_affinity(self, first, second):
        return self[frozenset((first, second))]

    def team_affinity(self, person, team):
        return sum(
            self[frozenset((person, member))] for member in team.members
        )

    def team_affinity_score(self, team):
        return sum(
            self[frozenset(pair)] for pair in combinations(team.members, 2)
        )


class TeamMaker:
    """
    Main class responsible for suggesting new teams.
    """
    def __init__(self, contestants, prev_games=None):
        self.contestants = set(contestants)
        self.games = prev_games or []

    def fill_in_players(self, game):
        if not game.teams:
            raise ValueError("Game needs to have at least one team.")

        affinities = self.get_contestant_affinities()
        scores = self.get_contestant_scores()
        remainders = self.contestants - game.get_participants()

        for person in sorted(
            sample(remainders, len(remainders)),
            key=lambda p: (
                p.sex,
                scores[p],
            ),
        ):
            min(
                game.teams,
                key=lambda t: (
                    len(t.members),
                    affinities.team_affinity(person, t),
                )
            ).members.append(person)

    def get_contestant_affinities(self):
        return Affinities.from_teams(
            chain.from_iterable(game.teams for game in self.games)
        )

    def get_contestant_scores(self):
        return sum((game.scores for game in self.games), start=Counter())
