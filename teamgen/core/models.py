from collections import Counter
from dataclasses import dataclass, field
from itertools import chain


@dataclass(eq=False)
class Person:
    """
    Represents single person participating any of games.
    """
    name: str
    sex: str


@dataclass(eq=False)
class Team:
    """
    Team encapsulates any number of players (persons participating a game).
    Players in one team usually cooperate against other teams.
    """
    name: str = None
    members: list[Person] = field(default_factory=list)


@dataclass(eq=False)
class Game:
    """
    Represents a game. For each game players (persons participating in game) are
    divided in any number of teams. Also game can assign score to players.
    """
    name: str
    teams: list[Team] = field(default_factory=list)
    scores: Counter[Person] = field(default_factory=Counter)

    @classmethod
    def create_empty(cls, name, number_of_teams, team_prefix=""):
        return cls(
            name=name,
            teams=[
                Team(name=f"{team_prefix}{i + 1}")
                for i in range(number_of_teams)
            ],
        )

    def get_participants(self):
        return set(chain.from_iterable(team.members for team in self.teams))
